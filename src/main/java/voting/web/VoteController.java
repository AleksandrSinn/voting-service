package voting.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import voting.web.dto.GetVoteStatsResponse;
import voting.web.dto.SaveVoteRequest;
import voting.web.dto.SaveVoteResponse;

import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

@RestController
@RequestMapping(path = "/votes")
public class VoteController {

    private final Set<UUID> userIds = new TreeSet<>();

    private int totalY = 0;

    private int totalN = 0;

    @PostMapping
    public SaveVoteResponse save(@RequestBody SaveVoteRequest request) {
        if (userIds.add(request.userId())) {
            switch (request.voteValue()) {
                case YES -> totalY++;
                case NO -> totalN++;
            }

            return new SaveVoteResponse(true);
        }

        return new SaveVoteResponse(false);
    }

    @GetMapping("/stats")
    public GetVoteStatsResponse getStats() {
        return new GetVoteStatsResponse(totalY, totalN);
    }
}
