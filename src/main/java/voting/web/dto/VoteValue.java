package voting.web.dto;

public enum VoteValue {
    YES, NO
}
