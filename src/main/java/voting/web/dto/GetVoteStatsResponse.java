package voting.web.dto;

public record GetVoteStatsResponse(int totalYes, int totalNo) {
}
