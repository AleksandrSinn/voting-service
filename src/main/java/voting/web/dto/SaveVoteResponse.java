package voting.web.dto;

public record SaveVoteResponse(boolean isSaved) {
}
