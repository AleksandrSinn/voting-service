package voting.web.dto;

import java.util.UUID;

public record SaveVoteRequest(UUID userId, VoteValue voteValue) {
}
