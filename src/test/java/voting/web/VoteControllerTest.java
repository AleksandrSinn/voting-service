package voting.web;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.boot.web.client.RestTemplateBuilder;
import voting.web.dto.GetVoteStatsResponse;
import voting.web.dto.SaveVoteRequest;
import voting.web.dto.SaveVoteResponse;
import voting.web.dto.VoteValue;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class VoteControllerTest {

    @LocalServerPort
    private int serverPort;

    @Test
    void saveVotes() {
        var httpClient = new RestTemplateBuilder()
                .rootUri("http://localhost:" + serverPort)
                .build();

        var newVotesCount = ThreadLocalRandom.current().nextInt(100, 1000);

        var newYesVotesCount = 0;

        var newNoVotesCount = 0;

        for (int i = 0; i < newVotesCount; i++) {
            final VoteValue voteValue;

            if (ThreadLocalRandom.current().nextBoolean()) {
                newYesVotesCount++;
                voteValue = VoteValue.YES;
            } else {
                newNoVotesCount++;
                voteValue = VoteValue.NO;
            }

            var request = new SaveVoteRequest(UUID.randomUUID(), voteValue);

            var response = httpClient.postForObject("/votes", request, SaveVoteResponse.class);

            assertTrue(response.isSaved());
        }

        var voteStatsResponse = httpClient.getForObject("/votes/stats", GetVoteStatsResponse.class);

        assertEquals(newYesVotesCount, voteStatsResponse.totalYes());
        assertEquals(newNoVotesCount, voteStatsResponse.totalNo());
    }
}